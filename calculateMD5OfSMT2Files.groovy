/*
 * Copyright (c) 2017, Venkatesh-Prasad Ranganath
 *
 * BSD 3-clause License
 *
 * Author: Venkatesh-Prasad Ranganath (rvprasad)
 */

import groovyx.gpars.GParsPool
import java.security.DigestInputStream
import java.security.MessageDigest

def generateMD5(final File file) {
    def md5 = MessageDigest.getInstance("MD5")
    file.withInputStream { is -> 
        def dis = new DigestInputStream(is, md5)
        while (dis.available()) {
            def len = Math.min(8192, dis.available())
            def buf = new byte[len]
            dis.read(buf, 0, len)
        }
    }
    md5.digest().encodeHex().toString()
}

def data = new File(".")
def checksum_folder = new File(data.canonicalPath + "/checksum/")
if (!checksum_folder.exists())
    checksum_folder.mkdir()

def dirName2fileNames = [:]
data.eachDirMatch(~/[A-Za-z0-9_-]+/, { dir ->
    def tmp2 = []
    dir.traverse(
        type: groovy.io.FileType.FILES,
        filter: { f -> (f ==~ /.*\.smt2/)},
        sort : { a -> a.getCanonicalFile()}) { f ->
            tmp2 << f.canonicalPath.drop(data.canonicalPath.length() + 1)
        }
    if (tmp2.size())
        dirName2fileNames[dir.name] = tmp2
})

GParsPool.withPool() {
    dirName2fileNames.eachParallel { dirName, fileNames ->
        def checksum_file = data.canonicalPath + "/checksum/" + dirName + "-md5.txt"
        def tmp2 = new File(checksum_file)
        if (tmp2.delete())
            tmp2.createNewFile()

        println "Processing $dirName"
        tmp2.withWriter { out ->
            fileNames.each { fileName ->
                // println "Processing $fileName"
                def md5 = generateMD5(new File("$data/$fileName"))
                out.println("$md5 $fileName")
            }
            println "Done : " + dirName
        }
    }
}
